<?php


if ( !function_exists('user_ip')) {

    /**
     * @return string UserIp
     */
    function user_ip() {
        return array_get('HTTP_CLIENT_ID', $_SERVER, '');
    }
}


if ( !function_exists('user_agent')) {

    /**
     * @return string UserAgent
     */
    function user_agent() {
        return array_get('HTTP_USER_AGENT', $_SERVER, '');
    }
}


if ( !function_exists('server_get')) {

    /**
     * @param array|string $key
     * @return mixed|null
     */
    function server_get($key) {
        return is_array($key) ? array_only($_GET, $key) : array_get($key, $_GET);
    }
}


if ( !function_exists('server_post')) {

    /**
     * @param array|string $key
     * @return mixed|null
     */
    function server_post($key) {
        return is_array($key) ? array_only($_POST, $key) : array_get($key, $_POST);
    }
}



if ( !function_exists('server_request')) {

    /**
     * @param array|string $key
     * @return mixed|null
     */
    function server_request($key) {
        return array_forge(server_get($key), server_post($key));
    }
}


