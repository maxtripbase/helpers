<?php



if ( !function_exists('config')) {


    /**
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    function config(string $key = '', $default = null)
    {
        return array_get($key, get_object_config(), $default);
    };
}



if ( !function_exists('get_object_config')) {


    /**
     * @return array
     */
    function get_object_config() {
        return call_user_func([trace_object(), 'getConfig']);
    }
}

