<?php


if ( !function_exists('json_response')) {

    /**
     * @param $response
     * @return bool
     */
    function json_response($response) {

        header('Content-Type: application/json');

        if (is_array($response) || is_object($response)) {
            $response = json_encode($response);
        }

        echo $response;

        return true;
    }
}