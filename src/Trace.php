<?php


if ( !function_exists('trace_object')) {

    /**
     * @return array
     */
    function trace_object() {
        foreach (debug_backtrace() as $trace) {
            if (array_get('object', $trace)) {
                return array_get('object', $trace);
            }
        }

        return null;
    }
}




if ( !function_exists('trace_config_dispatcher')) {

    /**
     * @return string
     */
    function trace_config_dispatcher() {
        foreach (debug_backtrace() as $trace) {
            if (array_get('function', $trace) === 'config') {
                return dirname(array_get('file', $trace));
            }
        }

        return null;
    }
}