<?php

if ( !function_exists('array_get')) {

    /**
     * @param string $key
     * @param array $array
     * @param null $default
     * @return mixed|null
     */
    function array_get(string $key, array $array = [], $default = null) {
        return array_key_exists($key, $array) ? $array[$key] : $default;
    }
}


if ( !function_exists('array_get_strict')) {

    /**
     * @param string $key
     * @param array $array
     * @return mixed
     * @throws Exception
     */
    function array_get_strict(string $key, array $array = []) {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }

        throw new Exception("No value found for {$key} in array");
    }
}


if ( !function_exists('array_push_if_exists')) {

    /**
     * @param array $array
     * @param null $item
     */
    function array_push_if_exists(array &$array, $item = null) {
        if ($item) {
            array_push($array, $item);
        }
    }
}

if ( !function_exists('array_only')) {

    /**
     * @param array $array
     * @param array $interest
     * @return array
     */
    function array_only(array $array, array $interest) {
        return array_intersect_key($array, array_flip($interest));
    }
}


if ( !function_exists('array_validate')) {

    function array_validate(array $array, array $fields) {
        return count($fields) === count(array_intersect_key($array, array_flip($fields)));
    }
}


if ( !function_exists('array_forge')) {

    /**
     * @param array|mixed $item1
     * @param array|mixed $item2
     * @return array
     */
    function array_forge($item1 = null, $item2 = null) {
        $item1 = is_array($item1) ? $item1 : [$item1];
        $item2 = is_array($item2) ? $item2 : [$item2];

        return array_merge($item1, $item2);
    }
}


if ( !function_exists('array_first')) {

    function array_first(array $array = [], $default = null) {
        return empty($array) ? $default : reset($array);
    }
}